import { Outlet } from "react-router-dom";
import Header from "../../../components/header/Header";
import Aside from "../../../components/aside/Aside";
import Footer from "../../../components/footer/Footer";
import classes from "./styles.module.scss";

const Layout = () => {
  return (
    <div>
      <div className={classes.container}>
        <div className={classes.content}>
          <Header />
          <Outlet />
        </div>
        <Aside />
      </div>
      <Footer />
    </div>
  );
};
export default Layout;
