import { Route, Routes } from "react-router-dom";
import { routes } from "./router/routes";
import MainPage from "./pages/MainPage";
import Layout from "./router/layouts/main/layout";

function App() {
  return (
    <Routes>
      <Route element={<Layout />}>
        {routes.map((route) => (
          <Route key={route.path} path={route.path} element={<route.Component />} />
        ))}
        <Route path="*" element={<MainPage />} />
      </Route>
    </Routes>
  );
}

export default App;
