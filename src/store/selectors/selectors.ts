import { createSelector } from "redux-orm";
import orm from "../models/models";

// Селектор для получения всех категорий
export const categoriesSelector = createSelector(orm, (session) => {
  return session.Category.all()
    .toModelArray()
    .map((category) => category.ref);
});

// Селектор для получения всех продуктов
export const productsSelector = createSelector(orm, (session) => {
  return session.Product.all()
    .toModelArray()
    .map((product) => product.ref);
});
