import { ORM, Model, attr, fk } from "redux-orm";

class Category extends Model {
  static get modelName() {
    return "Category";
  }

  static get fields() {
    return {
      id: attr(),
      name: attr(),
    };
  }
}

class Product extends Model {
  static get modelName() {
    return "Product";
  }

  static get fields() {
    return {
      id: attr(),
      name: attr(),
      categoryId: fk("Category", "products"),
      description: attr(),
      price: attr(),
    };
  }
}

const orm = new ORM();
orm.register(Category, Product);

export default orm;
