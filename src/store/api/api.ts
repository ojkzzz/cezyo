import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const api = createApi({
  reducerPath: "api",
  baseQuery: fetchBaseQuery({ baseUrl: "https://test2.sionic.ru/api/" }),
  endpoints: (builder) => ({
    getCategories: builder.query<any, void>({
      query: () => 'Categories?sort=["name","ASC"]&range=[0,24]',
    }),
    getProducts: builder.query<any, void>({
      query: () => 'Products?sort=["name","ASC"]&range=[0,24]',
    }),
  }),
});

export const { useGetCategoriesQuery, useGetProductsQuery } = api;
