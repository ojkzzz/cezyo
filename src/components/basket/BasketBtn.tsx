import BasketIcon from "../icons/BasketIcon";
import classes from "./styles.module.scss";

const BasketBtn = () => {
  const counter = "10+";
  return (
    <button className={classes.button}>
      <BasketIcon />
      <div className={classes.counter_container}>
        <p className={classes.counter_item}>{counter}</p>
      </div>
    </button>
  );
};
export default BasketBtn;
