import Card1 from "./card/card/card1/Card1";
import Card2 from "./card/card/card2/Card2";
import Card3 from "./card/card/card3/Card3";
import styles from "./styles.module.scss";

const Aside = () => {
  return (
    <div className={styles.container}>
      <Card1 />
      <Card2 />
      <Card3 />
      <Card2 />
    </div>
  );
};
export default Aside;
