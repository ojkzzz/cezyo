import CardTemplate from "../template/CardTemplate";
import classes from "./styles.module.scss";

const Card3 = () => {
  return (
    <CardTemplate>
      <button className={classes.button}>
        <p className={classes.title}>
          Новая <br /> коллекция
        </p>
      </button>
    </CardTemplate>
  );
};
export default Card3;
