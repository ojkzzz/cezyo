import CardTemplate from "../template/CardTemplate";
import SalesIcon from "../../../../icons/SalesIcon";
import classes from "./styles.module.scss";
import CustomButton from "../../../../button/CustomButton";

const Card1 = () => {
  return (
    <CardTemplate>
      <div className={classes.container}>
        <div className={classes.sales}>
          <SalesIcon />
        </div>
        <div className={classes.contentBox}>
          <p className={classes.title}>Получай товары БЕСПЛАТНО!</p>
          <CustomButton text="Узнать подробнее" onClick={() => {}} width="194px" />
        </div>
      </div>
    </CardTemplate>
  );
};
export default Card1;
