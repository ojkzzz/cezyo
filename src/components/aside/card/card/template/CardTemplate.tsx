import { FC, ReactNode } from "react";
import classes from "./styles.module.scss";

interface Props {
  children: ReactNode;
}

const CardTemplate: FC<Props> = ({ children }) => {
  return <div className={classes.card}>{children}</div>;
};
export default CardTemplate;
