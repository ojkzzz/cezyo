import classes from "./styles.module.scss";
import GooglePlayIcon from "../../icons/GooglePlayIcon";
import AppStore from "../../icons/AppStore";

const Apps = () => {
  return (
    <div className={classes.root}>
      <p className={classes.title}>Присоединяйтесь к нам</p>
      <div className={classes.links}>
        <a href="#">
          <GooglePlayIcon />
        </a>
        <a href="#">
          <AppStore />
        </a>
      </div>
    </div>
  );
};
export default Apps;
