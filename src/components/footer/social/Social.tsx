import classes from "./styles.module.scss";
import FacebookIcon from "../../icons/FacebookIcon";
import VkIcon from "../../icons/VkIcon";
import InstIcon from "../../icons/InstIcon";

const Social = () => {
  return (
    <div className={classes.root}>
      <p className={classes.title}>Присоединяйтесь к нам</p>
      <div className={classes.links}>
        <a href="#">
          <FacebookIcon />
        </a>
        <a href="#">
          <VkIcon />
        </a>
        <a href="#">
          <InstIcon />
        </a>
      </div>
    </div>
  );
};
export default Social;
