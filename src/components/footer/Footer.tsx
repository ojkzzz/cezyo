import Apps from "./apps/Apps";
import Social from "./social/Social";
import classes from "./styles.module.scss";

const Footer = () => {
  return (
    <footer className={classes.footer}>
      <div className={classes.top}>
        <h3 className={classes.title}>React</h3>
        <div className={classes.links}>
          <Social />
          <Apps />
        </div>
      </div>
      <div className={classes.bottom}>
        <a href="#" className={classes.link}>
          © Sionic
        </a>
        <a href="#" className={classes.link}>
          Правовая информация
        </a>
        <a href="#" className={classes.link}>
          Политика конфиденциальности
        </a>
      </div>
    </footer>
  );
};
export default Footer;
