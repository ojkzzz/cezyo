import classes from "./styles.module.scss";
import avatar from "../../assets/avatar.png";

const Avatar = () => {
  return (
    <button>
      <img className={classes.image} alt="avatar_image" src={avatar} />
    </button>
  );
};
export default Avatar;
