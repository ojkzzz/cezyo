import Avatar from "../avatar/Avatar";
import BasketBtn from "../basket/BasketBtn";
import Location from "../location/Location";
import Searching from "../search/Searching";
import classes from "./styles.module.scss";

const Header = () => {
  return (
    <header className={classes.container}>
      <h3 className={classes.title}>React</h3>
      <Location />
      <Searching />
      <BasketBtn />
      <Avatar />
    </header>
  );
};
export default Header;
