import { FC } from "react";
import { useGetProductsQuery } from "../../store/api/api";

const ProductList: FC = () => {
  const { data: products, error, isLoading } = useGetProductsQuery();

  if (isLoading) return <div>Loading...</div>;
  if (error) return <div>Error: {error.toString()}</div>;

  return (
    <div>
      <h1>Товары</h1>
      <ul>
        {products.map((product: any) => (
          <li key={product.id}>
            <h2>{product.name}</h2>
            <p>{product.description}</p>
            <p>Цена: {product.price}</p>
            <button>Добавить в корзину</button>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default ProductList;
