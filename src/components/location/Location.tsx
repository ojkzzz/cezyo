import GeoIcon from "../icons/GeoIcon";
import classes from "./styles.module.scss";

const Location = () => {
  //какая-то логика на получение локации
  const locationTitle = "Александровск-Са...";
  return (
    <button className={classes.container}>
      <GeoIcon />
      <p>{locationTitle}</p>
    </button>
  );
};
export default Location;
