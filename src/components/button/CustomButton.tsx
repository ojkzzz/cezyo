import { FC } from "react";
import classes from "./styles.module.scss";

interface Props {
  onClick: () => void;
  text: string;
  width?: string;
  height?: string;
}

const CustomButton: FC<Props> = ({ onClick, text, width, height }) => {
  return (
    <button className={classes.button} style={{ width, height }} onClick={onClick}>
      {text}
    </button>
  );
};
export default CustomButton;
