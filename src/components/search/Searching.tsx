import { FormEvent } from "react";
import SearchIcon from "../icons/SearchIcon";
import classes from "./styles.module.scss";

const Searching = () => {
  const onSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
  };
  return (
    <form onSubmit={onSubmit} className={classes.container}>
      <input
        type="text"
        className={classes.textInput}
        placeholder="Поиск бренда, товара, категории..."
      ></input>
      <button type="submit" className={classes.submitButton}>
        {" "}
        <SearchIcon />
      </button>
    </form>
  );
};
export default Searching;
