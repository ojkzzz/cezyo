import CategoryList from "../components/category/CategoryList";
import ProductList from "../components/product/ProductList";

const MainPage = () => {
  return (
    <div>
      <CategoryList />
      <ProductList />
    </div>
  );
};
export default MainPage;
